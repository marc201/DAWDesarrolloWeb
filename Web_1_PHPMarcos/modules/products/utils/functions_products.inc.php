<?php
function validate_products($value) {

    $error = array();
    $valido = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'provider' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^\D{2,30}$/')
        ),
        'location' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[a-z0-9- ]+$/i')
        ),
        'recycle' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^[0-9]{2,10}$/')
        ),
        'release_date' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp' => '/^(0[1-9]|[1-2][0-9]|3[0-1])\/(0[1-9]|1[0-2])\/[0-9]{4}$/')
        ),
    );
    

    $resultado = filter_var_array($value, $filtro);

    //no filter
    $resultado['taxes'] = $value['taxes'];
    $resultado['stocking'] = $value['stocking'];
    $resultado['country'] = $value['country'];
    $resultado['province'] = $value['province'];
    $resultado['city'] = $value['city'];
    $resultado['release_date'] = $value['release_date'];


    if ($resultado['taxes'] === 'Select taxes') {
        $error['taxes'] = "You haven't select taxes.";
        $valido = false;
    }

    if ($resultado != null && $resultado) {


        if (!$resultado['name']) {
            $error['name'] = 'Name must be 2 to 20 letters';
            $valido = false;
        }

        if (!$resultado['recycle']) {
            $error['recycle'] = 'User must be 2 to 10 whole number';
            $valido = false;
        }

        if (!$resultado['location']) {
            $error['location'] = "The Location don't have points or symbols.";
            $valido = false;
        }

        if (!$resultado['provider']) {
            $error['provider'] = 'The provider must be 2 to 30 letters';
            $valido = false;
        }
        if (!$resultado['release_date']) {
            if ($resultado['release_date'] == "") {
                $error['release_date'] = "Release date can't be empty";
                $valid = false;
            } else {
                $error['release_date'] = 'error realease format date (dd/mm/yyyy)';
                $valid = false;
            }
        }

    } else {
        $valido = false;
    };
    /*
        if ((isset($_POST['alta_users_json']))) {
       $jsondata["success"] = $value;
            echo json_encode($jsondata);
            die();
        }
    */
    return $return = array('resultado' => $valido, 'error' => $error, 'datos' => $resultado);

}

//validate email
function valida_email($email) {
    $email = filter_var($email, FILTER_SANITIZE_EMAIL);
    if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
        if (filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp' => '/^.{5,50}$/')))) {
            return $email;
        }
    }
    return false;
}
