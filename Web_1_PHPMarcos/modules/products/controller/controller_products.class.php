<?php  
class controller_products {
     function __construct() { 
        //session_start();
        
        include (UTILS_PRODUCTS."functions_products.inc.php");

        include (UTILS."upload.php");

        //include (UTILS."common.inc.php");
    
        $_SESSION['module'] = "products";
        
    }
    function form_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br><br><br><br><br><br><br>';
        loadView('modules/products/view/', 'create_products.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function results_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        echo '<br><br><br><br><br><br><br>';
        loadView('modules/products/view/', 'results_products.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

//////////////////////////////////////////////////////////////// upload
    function upload_products() {
        if ((isset($_POST["upload"])) && ($_POST["upload"] == true)) {
            $result_avatar = upload_files();
            $_SESSION['result_avatar'] = $result_avatar;
            //echo debug($_SESSION['result_avatar']); //se mostraría en alert(response); de dropzone.js
        }
    }
//////////////////////////////////////////////////////////////// alta_users_json
    function alta_products() {

            if ((isset($_POST['alta_users_json']))) {
            $jsondata = array();
            $usersJSON = json_decode($_POST["alta_users_json"], true);
            $result = validate_products($usersJSON);

            if (empty($_SESSION['result_avatar'])) {
                $_SESSION['result_avatar'] = array('resultado' => true, 'error' => "", 'datos' => 'media/default-avatar.png');
            }
            $result_avatar = $_SESSION['result_avatar'];

           if (($result['resultado']) && ($result_avatar['resultado'])) {
                $arrArgument = array(
                    'name' => ucfirst($result['datos']['name']),
                    'provider' => ucfirst($result['datos']['provider']),
                    'location' => $result['datos']['location'],
                    'recycle' => $result['datos']['recycle'],
                    'taxes' => $result['datos']['taxes'],
                    'stocking' => $result['datos']['stocking'],
                    'country' => $result['datos']['country'],
                    'province' => $result['datos']['province'],
                    'city' => $result['datos']['city'],
                    'release_date' => $result['datos']['release_date'],
                    'avatar' => $result_avatar['datos']
                );
               
                 $arrValue = false;
                //try {
                //$path_model = $_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products/model/model/';
                $arrValue = loadModel(MODEL_PRODUCTS, "products_model", "create_product", $arrArgument);
                //} catch (Exception $e) {
                //    showErrorPage(2, "ERROR - 503 BD", 'HTTP/1.0 503 Service Unavailable', 503);
                //}

                //echo json_encode($arrValue);
                //die();

                if ($arrValue)
                    $mensaje = "Su registro se ha efectuado correctamente, para finalizar compruebe que ha recibido un correo de validacion y siga sus instrucciones";
                else
                    $mensaje = "No se ha podido realizar su alta. Intentelo mas tarde";


                $_SESSION['user'] = $arrArgument;
                $_SESSION['msje'] = $mensaje;
                $callback = "../../products/results_products/";//index.php?module=products&view=results_products


                $jsondata["success"] = true;
                $jsondata["redirect"] = $callback;
                echo json_encode($jsondata);
                exit;
            } else  {
                //$error = $result['error'];
                //$error_avatar = $result_avatar['error'];
                $jsondata["success"] = false;
                $jsondata["error"] = $result['error'];
                $jsondata["error_avatar"] = $result_avatar['error'];

                $jsondata["success1"] = false;
                if ($result_avatar['resultado']) {
                    $jsondata["success1"] = true;
                    $jsondata["img_avatar"] = $result_avatar['datos'];
                }
                header('HTTP/1.0 400 Bad error');
                echo json_encode($jsondata);
                exit;
            }
        }
    }

    function delete_products() {
        if (isset($_POST["delete"]) && $_POST["delete"] == true) {
            $_SESSION['result_avatar'] = array();
            $result = remove_files();
            if ($result === true) {
                echo json_encode(array("res" => true));
            } else {
                echo json_encode(array("res" => false));
            }
        }
    }

//////////////////////////////////////////////////////////////// load
    function load_products() {
        if (isset($_POST["load"]) && $_POST["load"] == true) {
            $jsondata = array();
            if (isset($_SESSION['user'])) {
                //echo debug($_SESSION['user']);
                $jsondata["user"] = $_SESSION['user'];
            }
            if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }
            close_session();
            echo json_encode($jsondata);
            exit;
        }
    }  

/////////////////////////////////////////////////// load_data
    function load_data_products() {
        echo ("Dentro");
        if ((isset($_POST["load_data"])) && ($_POST["load_data"] == true)) {
            $jsondata = array();
            if (isset($_SESSION['user'])) {
                $jsondata["user"] = $_SESSION['user'];
                echo json_encode($jsondata);
                exit;
            } else {
                $jsondata["user"] = "";
                echo json_encode($jsondata);
                exit;
            }
        }
    }
/////////////////////////////////////////////////// load_country

    function load_countries_products() {
        if(  (isset($_POST["load_country"])) && ($_POST["load_country"] == true)  ){
                $json = array();

                $url = 'http://www.oorsprong.org/websamples.countryinfo/CountryInfoService.wso/ListOfCountryNamesByName/JSON';
                //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products/model/model/';
                $json = loadModel(MODEL_PRODUCTS, "products_model", "obtain_countries", $url);
                
                if($json){
                    echo $json;
                    exit;
                }else{
                    $json = "error";
                    echo $json;
                    exit;
                }
            }
    }
/////////////////////////////////////////////////// load_provinces
    function load_provinces_products() {
        if(  (isset($_POST["load_provinces"])) && ($_POST["load_provinces"] == true)  ){
                $jsondata = array();
                $json = array();

                //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products/model/model/';
                $json = loadModel(MODEL_PRODUCTS, "products_model", "obtain_provinces");

                if($json){
                    $jsondata["provinces"] = $json;
                    echo json_encode($jsondata);
                    exit;
                }else{
                    $jsondata["provinces"] = "error";
                    echo json_encode($jsondata);
                    exit;
                }
            }
    }
/////////////////////////////////////////////////// load_cities
    function load_towns_products() {
        if(  isset($_POST['idPoblac']) ){
                $jsondata = array();
                $json = array();

                //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products/model/model/';
                $json = loadModel(MODEL_PRODUCTS, "products_model", "obtain_cities", $_POST['idPoblac']);

                if($json){
                    $jsondata["cities"] = $json;
                    echo json_encode($jsondata);
                    exit;
                }else{
                    $jsondata["cities"] = "error";
                    echo json_encode($jsondata);
                    exit;
                }
            }
    }
}