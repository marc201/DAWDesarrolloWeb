
jQuery.fn.fill_or_clean = function () {
    this.each(function () {
        //if ($("#name").val() == "") {
        if ($("#name").val() == "") {
            $("#name").val("Introduce name");
            $("#name").focus(function () {
                if ($("#name").val() == "Introduce name") {
                    $("#name").val('');
                }
            });
        }
        $("#name").blur(function () { //Onblur se activa cuando el usuario retira el foco
            if ($("#name").val() == "") {
                $("#name").val("Introduce name");
            }
        });

        if ($("#provider").val() == "") {
            $("#provider").val("Introduce last name");
            $("#provider").focus(function () {
                if ($("#provider").val() == "Introduce last name") {
                    $("#provider").val('');
                }
            });
        }
        $("#provider").blur(function () {
            if ($("#provider").val() == "") {
                $("#provider").val("Introduce last name");
            }
        });
        
        if ($("#location").val() == "") {
            $("#location").val( "Introduce location");
            $("#location").focus(function () {
                if ($("#location").val() == "Introduce location") {
                    $("#location").val( "");
                }
            });
        }
        $("#location").blur(function () {
            if ($("#location").val() == "") {
                $("#location").val( "Introduce location");
            }
        });
        if ($("#recycle").val() == "") {
            $("#recycle").val( "recycle");
            $("#recycle").focus(function () {
                if ($("#recycle").val() == "recycle") {
                    $("#recycle").val( "");
                }
            });
        }
        $("#recycle").blur(function () {
            if ($("#recycle").val() == "") {
                $("#recycle").val( "recycle");
            }
        });
        
       if ($("#taxes").val() == "") {
            $("#taxes").val( "taxes");
            $("#taxes").focus(function () {
                if ($("#taxes").val() == "Selecciona el nivel") {
                    $("#taxes").val( "");
                }
            });
        }
        $("#taxes").blur(function () {
            if ($("#taxes").val() == "") {
                $("#taxes").val( "Selecciona el nivel");
            }
        });
        if ($("#release_date").val() === "") {
            $("#release_date").val("Input release date");
            $("#release_date").focus(function () {
                if ($("#release_date").val() === "Input release date") {
                    $("#release_date").val("");
                }
            });
        }
        $("#release_date").blur(function () { //Onblur is activated when user changes the focus
            if ($("#release_date").val() === "") {
                $("#release_date").val("Input release date");
            }
        });//release_date end
        
    });//each
    return this;
};//function

Dropzone.autoDiscover = false;
$(document).ready(function () {


 //Datepicker///////////////////////////

    $( "#release_date" ).datepicker({
         dateFormat: 'dd/mm/yy'
    });
    

 console.log("redy!");
    //Valida Products /////////////////////////
    $('#submit_product').click(function () {
        console.log("Hola");
        validate_user();
    });

    //Control de seguridad para evitar que al volver atrás de la pantalla results a create, no nos imprima los datos
    $.post("../../products/load_data_products/",{'load_data':true},
    //$.get("modules/products/controller/controller_products.class.php?load_data=true",
            function (response) {
                //alert(response.user);
                if (response.user === "") {
                    $("#name").val('');
                    $("#provider").val('');
                    $("#location").val('');
                    $("#recycle").val('');
                    $("#release_date").val('');
                    $('#country').val('Select country');
                    $('#province').val('Select province');
                    $('#city').val('Select city');
                    $("#taxes").val('Select level');
                    var inputElements = document.getElementsByClassName('messageCheckbox');
                    for (var i = 0; i < inputElements.length; i++) {
                        if (inputElements[i].checked) {
                            inputElements[i].checked = false;
                        }
                    }
                    //siempre que creemos un plugin debemos llamarlo, sino no funcionará
    $(this).fill_or_clean();
                } else {
                    $("#name").val( response.user.name);
                    $("#provider").val( response.user.provider);
                    $("#location").val( response.user.location);
                    $("#recycle").val( response.user.recycle);
                    $("#taxes").val( response.user.taxes);
                    $("#release_date").val(response.product.release_date);
                    $('#country').val(response.product.country);
                    $('#province').val(response.product.province);
                    $('#city').val(response.product.city);
                    /*var interests = response.user.interests;
                    var inputElements = document.getElementsByClassName('messageCheckbox');
                    for (var i = 0; i < interests.length; i++) {
                        for (var j = 0; j < inputElements.length; j++) {
                            if(interests[i] ===inputElements[j] )
                                inputElements[j].checked = true;
                        }
                    }
                    */
                }
            }, "json");
    
    
    //Dropzone function //////////////////////////////////
    $("#dropzone").dropzone({
        url: "../../products/upload_products/",
        params:{'upload':true},
        addRemoveLinks: true,
        maxFileSize: 1000,
        dictResponseError: "Ha ocurrido un error en el server",
        acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd',
        init: function () {
            this.on("success", function (file, response) {
                //alert(response);
                $("#progress").show();
                $("#bar").width('100%');
                $("#percent").html('100%');
                $('.msg').text('').removeClass('msg_error');
                $('.msg').text('Success Upload image!!').addClass('msg_ok').animate({'right': '300px'}, 300);
            });
        },
        complete: function (file) {
            //if(file.status == "success"){
            //alert("El archivo se ha subido correctamente: " + file.name);
            //}
        },
        error: function (file) {
            //alert("Error subiendo el archivo " + file.name);
        },
        removedfile: function (file, serverFileName) {
            var name = file.name;
            $.ajax({
                type: "POST",
                url: "../../products/delete_products/",
                data: {"filename":name,"delete":true},
                success: function (data) {
                    $("#progress").hide();
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    $("#e_avatar").html("");

                    var json = JSON.parse(data);
                    if (json.res === true) {
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                            //alert("Imagen eliminada: " + name);
                        } else {
                            false;
                        }
                    } else { //json.res == false, elimino la imagen también
                        var element;
                        if ((element = file.previewElement) != null) {
                            element.parentNode.removeChild(file.previewElement);
                        } else {
                            false;
                        }
                    }
                }
            });
        }
    });

    //Utilizamos las expresiones regulares para las funciones de  fadeout
    var email_reg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var date_reg = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var location_reg = /^[a-z0-9- -.]+$/i;
    var recycle_reg = /^[0-9a-zA-Z]{6,32}$/;
    var string_reg = /^[A-Za-z]{2,30}$/;
    var usr_reg = /^[0-9a-zA-Z]{2,20}$/;

    //realizamos funciones para que sea más práctico nuestro formulario
    $("#name, #provider").keyup(function () {
        if ($(this).val() != "" && string_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#recycle").keyup(function () {
        if ($(this).val() != "" && recycle_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    $("#location").keyup(function () {
        if ($(this).val() != "" && location_reg.test($(this).val())) {
            $(".error").fadeOut();
            return false;
        }
    });

    //Dependent combos //////////////////////////////////
    load_countries_v1();
    
    $("#province").empty();
    $("#province").append('<option value="" selected="selected">Select province</option>');
    $("#province").prop('disabled', true);
    $("#city").empty();
    $("#city").append('<option value="" selected="selected">Select city</option>');
    $("#city").prop('disabled', true);

    $("#country").change(function() {
        var country = $(this).val();
        var province = $("#province");
        var city = $("#city");

        if(country !== 'ES'){
             province.prop('disabled', true);
             city.prop('disabled', true);
             $("#province").empty();
             $("#city").empty();
        }else{
             province.prop('disabled', false);
             city.prop('disabled', false);
             load_provinces_v1();
        }//fi else
    });

    $("#province").change(function() {
        var prov = $(this).val();
        if(prov > 0){
            load_cities_v1(prov);
        }else{
            $("#city").prop('disabled', false);
        }
    });


function validate_user() {
    console.log("Validate")
    var result = true;

    var name = document.getElementById('name').value;
     console.log(" "+name);
    var provider = document.getElementById('provider').value;
    var location = document.getElementById('location').value;
    var taxes = document.getElementById('taxes').value;
    var recycle = document.getElementById('recycle').value;

    var idstoking = document.getElementsByName('stocking');

    var country = document.getElementById('country').value;
    var province = document.getElementById('province').value;
    var city = document.getElementById('city').value;

    for(var i=0;i<idstoking.length;i++)
        {
            if(idstoking[i].checked)
                valueStoking=idstoking[i].value;
        }

    var stocking = valueStoking;

    var release_date = document.getElementById('release_date').value;

    //document.getElementById('stocking').value;
    /*
    var interests = [];
    var inputElements = document.getElementsByClassName('messageCheckbox');
    var j = 0;
    for (var i = 0; i < inputElements.length; i++) {
        if (inputElements[i].checked) {
            interests[j] = inputElements[i].value;
            j++;
        }
    }
    */
    //Utilizamos las expresiones regulares para la validación de errores JS
    var email_reg = /^[a-zA-Z0-9_\.\-]+@[a-zA-Z0-9\-]+\.[a-zA-Z0-9\-\.]+$/;
    var date_reg = /^(0[1-9]|1[012])[- \/.](0[1-9]|[12][0-9]|3[01])[- \/.](19|20)\d\d$/;
    var location_reg = /^[a-z0-9- -.]+$/i;
    var recycle_reg = /^[0-9]{1,32}$/;
    var string_reg = /^[A-Za-z]{2,30}$/;
    var usr_reg = /^[0-9a-zA-Z]{2,20}$/;
    var val_dates = /\d{2}.\d{2}.\d{4}$/;

    $(".error").remove();
    
    if ($("#name").val() == "" || $("#name").val() == "Introduce name") {
        $("#name").focus().after("<span class='error'>Introduce name</span>");
        result = false;
        return false;
    } else if (!string_reg.test($("#name").val())) {
        $("#name").focus().after("<span class='error'>Name must be 2 to 30 letters</span>");
        result = false;
        return false;
    }

    else if ($("#provider").val() == "" || $("#provider").val() == "Introduce last name") {
        $("#provider").focus().after("<span class='error'>Introduce last name</span>");
        result = false;
        return false;
    } else if (!string_reg.test($("#provider").val())) {
        $("#provider").focus().after("<span class='error'>Last name must be 2 to 30 letters</span>");
        result = false;
        return false;
    }

    if ($("#location").val() == "" || $("#location").val() == "Introduce location") {
        $("#location").focus().after("<span class='error'>Introduce location</span>");
        result = false;
        return false;
    } else if (!location_reg.test($("#location").val())) {
        $("#location").focus().after("<span class='error'>location don't have  symbols.</span>");
        result = false;
        return false;
    }

    if ($("#recycle").val() == "" || $("#recycle").val() == "recycle") {
        $("#recycle").focus().after("<span class='error'>Introduce recycle</span>");
        result = false;
        return false;
    } else if (!recycle_reg.test($("#recycle").val())) {
        $("#recycle").focus().after("<span class='error'>Recycle must be 1 to 32 characters.</span>");
        result = false;
        return false;
    }

    else if ($("#taxes").val() == "" || $("#taxes").val() == "Selecciona el nivel") {
        $("#taxes").focus().after("<span class='error'>Selecciona el nivel</span>");
        result = false;
        return false;
    }

    if ($("#country").val() === "" || $("#country").val() === "Select country" || $("#country").val() === null) {
        $("#country").focus().after("<span class='error'>Select one country</span>");
        return false;
    }

    if ($("#province").val() === "" || $("#province").val() === "Select province") {
        $("#province").focus().after("<span class='error'>Select one province</span>");
        return false;
    }

    if ($("#city").val() === "" || $("#city").val() === "Select city") {
        $("#city").focus().after("<span class='error'>Select one city</span>");
        return false;
    }
    if ($("#release_date").val() === "" || $("#release_date").val() === "Input release date") {
        $("#release_date").focus().after("<span class='error'>JS Input product release date</span>");
        return false;
    } else if (!val_dates.test($("#release_date").val())) {
        $("#release_date").focus().after("<span class='error'>JS Input product release date</span>");
        return false;
    }


    //Si ha ido todo bien, se envian los datos al servidor
    if (result) {

         if (province === null) {
            province = 'default_province';
        }else if (province.length === 0) {
            province = 'default_province';
        }else if (province === 'Select province') {
            return 'default_province';
        }

        if (city === null) {
            city = 'default_city';
        }else if (city.length === 0) {
            city = 'default_city';
        }else if (city === 'Select city') {
            return 'default_city';
        }

        
        var data = {"name": name, "provider": provider, "location": location, "taxes": taxes, "recycle": recycle, "stocking": stocking,"country": country, "province": province, "city": city, "release_date": release_date};
            
        var data_users_JSON = JSON.stringify(data);


        $.post('../../products/alta_products/', {alta_users_json: data_users_JSON},
        function (response) {
            console.log(typeof(response));
            if (response.success) {
                window.location.href = response.redirect;
            }
            console.log(response);
        }, "json")
                .fail(function (xhr, textStatus, errorThrown) {
                    if (xhr.responseJSON === undefined || xhr.responseJSON === null)
                        xhr.responseJSON = JSON.parse(xhr.responseText);
                    /*
                    if (xhr.status === 0) {
                        alert('Not connect: Verify Network.');
                    } else if (xhr.status === 404) {
                        alert('Requested page not found [404]');
                    } else if (xhr.status === 500) {
                        alert('Internal Server Error [500].');
                    } else if (textStatus === 'parsererror') {
                        alert('Requested JSON parse failed.');
                    } else if (textStatus === 'timeout') {
                        alert('Time out error.');
                    } else if (textStatus === 'abort') {
                        alert('Ajax request aborted.');
                    } else {
                        alert('Uncaught Error: ' + xhr.responseText);
                    }
                    */
                    if (xhr.responseJSON !== undefined && xhr.responseJSON !== null) {

                        if (xhr.responseJSON.error.name !== undefined && xhr.responseJSON.error.name !== null) {
                            $("#name").focus().after("<span class='error'>" + xhr.responseJSON.error.name + "</span>");
                        }

                        if (xhr.responseJSON.error.provider !== undefined && xhr.responseJSON.error.provider !== null) {
                            $("#provider").focus().after("<span class='error'>" + xhr.responseJSON.error.provider + "</span>");
                        }
                        if (xhr.responseJSON.error.location !== undefined && xhr.responseJSON.error.location !== null) {
                            $("#location").focus().after("<span class='error'>" + xhr.responseJSON.error.location + "</span>");
                        }
                        if (xhr.responseJSON.error.taxes !== undefined && xhr.responseJSON.error.taxes !== null) {
                            $("#taxes").focus().after("<span class='error'>" + xhr.responseJSON.error.taxes + "</span>");
                        }
                        if (xhr.responseJSON.error.recycle !== undefined && xhr.responseJSON.error.recycle !== null) {
                            $("#recycle").focus().after("<span class='error'>" + xhr.responseJSON.error.recycle + "</span>");
                        }
                         if(xhr.responseJSON.error.country !== undefined && xhr.responseJSON.error.recycle !== null) {
                            $("#error_country").focus().after("<span  class='error1'>" + xhr.responseJSON.error.country + "</span>");
                        }
                         if(xhr.responseJSON.error.province !== undefined && xhr.responseJSON.error.recycle !== null) {
                            $("#error_province").focus().after("<span  class='error1'>" + xhr.responseJSON.error.province + "</span>");
                        }
                            if(xhr.responseJSON.error.city !== undefined && xhr.responseJSON.error.recycle !== null) {
                         $("#error_city").focus().after("<span  class='error1'>" + xhr.responseJSON.error.city + "</span>");
                        }
          
                        if (xhr.responseJSON.error.stocking !== undefined && xhr.responseJSON.error.stocking !== null) {
                            $("#stocking").focus().after("<span class='error'>" + xhr.responseJSON.error.stocking + "</span>");
                        }
                        
                        if (xhr.responseJSON.error_avatar !== undefined && xhr.responseJSON.error_avatar !== null) {
                            $("#avatar").focus().after("<span class='error'>" + xhr.responseJSON.error_avatar + "</span>");
                        }
                        

                    }
                    
                    if (!(xhr.responseJSON.success1)) {
                        $("#bar").width('0%');
                        $("#percent").html('0%');
                        $('.msg').text('').removeClass('msg_ok');
                        $('.msg').text('Error Upload image!!').addClass('msg_error').animate({'right': '300px'}, 300);
                    }
                    
                });
}
}
});// end products.js

function load_countries_v2(cad, post_data) {
    $.post(cad, post_data, function (data) {
        $("#country").empty();
        $("#country").append('<option value="" selected="selected">Select Country</option>');
        $.each(data, function (i, valor) {
            $("#country").append("<option value='" + valor.sISOCode + "'>" + valor.sName + "</option>");
        });
    },'json')
            .fail(function () {
                alert("error load_countries");
            });
}

function load_countries_v1() {
    console.log("Load");
    //$.get( "modules/products/controller/controller_products.class.php?load_country=true",
    $.post("../../products/load_countries_products/",{'load_country':true},
        function( response ) {
            //console.log(response);
            if(response === 'error'){
                load_countries_v2("../../resources/ListOfCountryNamesByName.json");
            }else{
                load_countries_v2("../../products/load_countries_products/",{'load_country':true}); //oorsprong.org
            }
    })
    .fail(function(response) {
        load_countries_v2("../../resources/ListOfCountryNamesByName.json");
    });
}

function load_provinces_v2() {
    $.get("../../resources/provinciasypoblaciones.xml", function (xml) {
        $("#province").empty();
        $("#province").append('<option value="Select Province" selected="selected">Select Province</option>');

        $(xml).find("provincia").each(function () {
            var id = $(this).attr('id');
            var nombre = $(this).find('nombre').text();
            $("#province").append("<option value='" + id + "'>" + nombre + "</option>");
        });
    })
            .fail(function () {
                alert("error load_provinces");
    });
}

function load_provinces_v1() { //provinciasypoblaciones.xml - xpath
    //$.get( "modules/products/controller/controller_products.class.php?load_provinces=true",
    $.post('../../products/load_provinces_products/',{'load_provinces':true},
        function( response ) {
          $("#province").empty();
            $("#province").append('<option value="" selected="selected">Select province</option>');

            //alert(response);
        var json = JSON.parse(response);
            var provinces=json.provinces;
            //alert(provinces);
            //console.log(provinces);

            //alert(provinces[0].id);
            //alert(provinces[0].nombre);

            if(provinces === 'error'){
                load_provinces_v2();
            }else{
                for (var i = 0; i < provinces.length; i++) {
                    $("#province").append("<option value='" + provinces[i].id + "'>" + provinces[i].nombre + "</option>");
                }
            }
    })
    .fail(function(response) {
        load_provinces_v2();
    });
}

function load_cities_v2(prov) {
    $.get("../../resources/provinciasypoblaciones.xml", function (xml) {
        $("#town").empty();
        $("#town").append('<option value="Select Town" selected="selected">Select Town</option>');

        $(xml).find('provincia[id=' + prov + ']').each(function () {
            $(this).find('localidad').each(function () {
                $("#town").append("<option value='" + $(this).text() + "'>" + $(this).text() + "</option>");
            });
        });
    })
            .fail(function () {
                alert("error load_towns");
            });
}

function load_cities_v1(prov) { //provinciasypoblaciones.xml - xpath
    var datos = { idPoblac : prov  };
    
    $.post("../../products/load_towns_products/", datos, function(response) {
        //alert(response);
        var json = JSON.parse(response);
        var cities=json.cities;
        //alert(poblaciones);
        //console.log(poblaciones);
        //alert(poblaciones[0].poblacion);

        $("#city").empty();
        $("#city").append('<option value="" selected="selected">Select city</option>');

        if(cities === 'error'){
            load_cities_v2(prov);
        }else{
            for (var i = 0; i < cities.length; i++) {
                $("#city").append("<option value='" + cities[i].poblacion + "'>" + cities[i].poblacion + "</option>");
            }
        }
    })
    .fail(function() {
        load_cities_v2(prov);
    });
}