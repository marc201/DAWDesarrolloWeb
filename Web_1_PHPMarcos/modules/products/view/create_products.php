<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/3.51/jquery.form.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/min/dropzone.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/dropzone/4.0.1/dropzone.css">
<!-- Script with absolute route -->
<script type="text/javascript" src="<?php echo PRODUCTS_JS_PATH ?>products.js" ></script>

<section id="contact-page">
    <div class="container">
        <div class="center">        
            <h2>ADD NEW PRODUCT </h2>
            <p class="lead">Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
        </div> 
        <div class="row contact-wrap"> 
            <div class="status alert alert-success" style="display: none"></div>
            <form id="form_products" name="form_products">
            <div class ="form-group">
                    <input type="hidden" name="alta_products" value="alta_products">
                </div>
                <div class="col-sm-5 col-sm-offset-1">
                    <div class="form-group">
                        <label>Nombre *</label>
                        <input type="text" id="name" name="name" placeholder="name" class="form-control" required="required" value="Hola" >
                        <div id="e_name"></div>
                    </div>
                    <div class="form-group">
                        <label>Provider *</label>
                        <input type="text" id="provider" name="provider" placeholder="provider" class="form-control" required="required" value="Defauld">
                        <div id="e_provider"></div>
                    </div>
                    <div class="form-group">
                        <label>Location</label>
                        <input id="location" type="text" name="location" placeholder="hall shelving and height" required="required" class="form-control" value="1A27">
                            <div id="e_location"></div>
                    </div>
                    

                    <div class="form-group">
                        <label>Taxes</label>
                        <select name="taxes" id="taxes">
                            <option selected>Select level</option>
                            <option value="21">Normal (21%)</option>
                            <option value="10">Reduced (10%)</option>
                            <option value="4">Extra Reduced (4%)</option>
                            <option value="0">Exent (0%)</option>
                        </select>
                         <div id="e_taxes"></div>
                    </div>

                </div>
                <div class="col-sm-5">
                    <div class="form-group">
                        <label>Taxes for recycle</label>
                        <input type="number" id="recycle" name="recycle" placeholder="recycle" class="form-control" required="required" value="22">
                        <div id="e_recycle"></div>
                    </div>
                    
                    <div class="form-group">
                        <label>Release date *</label>
                        <input id="release_date" type="text" name="release_date" class = "release_date" placeholder="mm/dd/yyyy" value=""  class="form-control" >
                        <div id="e_entry_date"></div>
                    </div>
                    
                    <div>
                        <label>Country: </label>
                            <select name="country" id="country">
                            <option selected>Select country</option>
                            </select>
                            <div id="error_country"></div>
                            <div></div>
                    </div>
                    <div>
                        <label>Province: </label>
                            <select name="province" id="province">
                            <option selected>Select province</option>
                            </select>
                            <div id="error_province"></div>
                            <div></div>
                    </div>
                    <div>
                        <label>City: </label>
                            <select name="city" id="city">
                            <option selected>Select city</option>
                            </select>
                            <div id="error_city"></div>
                            <div></div>
                    </div>

                    <div class="form-group">
                        <label>Days for stocking  *</label>

                        Super A <input type="radio" name="stocking" value="Super A"> <br>
                        A <input type="radio" name="stocking" value="A"> <br>
                        B <input type="radio" name="stocking" value="B" checked> <br>
                        C <input type="radio" name="stocking" value="C"> <br>
                        Super C <input type="radio" name="stocking" value="Super C"> <br>

                    </div> 

                    <!--
                     <div class="form-group">
                        <label>Type of the Beer </label>

                        Lager  <input type="radio" name="stocking" value="Lager"> <br>
                        Ale  <input type="radio" name="stocking" value="Ale"> <br>
                        Lambic  <input type="radio" name="stocking" value="Lambic"> <br>
                        Cider   <input type="radio" name="stocking" value="Cider"> <br>
                        Darck   <input type="radio" name="stocking" value="Darck"> <br>
                        Other   <input type="radio" name="stocking" value="Other" checked> <br>

                    </div> 
                    -->
                    <br />
                    <br />
                    <br />
                    <br />
                    <div class="form-group" id="progress">
                        <div id="bar"></div>
                        <div id="percent">0%</div >
                    </div>

                    <div class="msg"></div>
                    <br/>
                    <div id="dropzone" class="dropzone"></div><br/>
                    <br/>
                    <br/>
                    <br/>
                    <br>
                    <div class="form-group">
                        <button type="button" id="submit_product" name="submit_product" class="btn btn-primary btn-lg" value="submit_product">Submit Message</button>
                    </div>
                </div>
            </form> 
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#contact-page-->