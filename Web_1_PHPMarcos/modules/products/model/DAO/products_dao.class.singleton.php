<?php
class products_DAO {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function create_product_DAO($db, $arrArgument) {
        $name = $arrArgument['name'];
        $provider = $arrArgument['provider'];
        //$birth_date = $arrArgument['birth_date'];
        //$title_date = $arrArgument['title_date'];
        $location = $arrArgument['location'];
        //$user = $arrArgument['user'];
        //$pass = $arrArgument['pass'];
        $recycle = $arrArgument['recycle'];
        $taxes = $arrArgument['taxes'];
        $stocking = $arrArgument['stocking'];
        $avatar = $arrArgument['avatar'];
        $country = $arrArgument['country'];
        $province = $arrArgument['province'];
        $city = $arrArgument['city'];
        $release_date = $arrArgument['release_date'];

        /*
        $history = 0;
        $music = 0;
        $computing = 0;
        $magic = 0;

        foreach ($stocking as $indice) {
            if ($indice === 'History')
                $history = 1;
            if ($indice === 'Music')
                $music = 1;
            if ($indice === 'Computing')
                $computing = 1;
            if ($indice === 'Magic')
                $magic = 1;
        }
        */

        //echo json_encode($arrArgument);
        //       die();
        
        $sql = "INSERT INTO products (name, provider,"
                . " location, recycle, taxes, stocking, avatar, country, province, city, release_date"
                . " ) VALUES ('$name', '$provider',"
                . " '$location','$recycle', '$taxes', '$stocking', '$avatar','$country','$province','$city', '$release_date')";
        
                // echo json_encode($db->ejecutar($sql));
                //die();

        return $db->ejecutar($sql);
    }
    public function obtain_countries_DAO($url){
          $ch = curl_init();
          curl_setopt ($ch, CURLOPT_URL, $url);
          curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, 5);
          $file_contents = curl_exec($ch);

          $httpcode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
          curl_close($ch);
          $accepted_response = array(200, 301, 302);
          if(!in_array($httpcode, $accepted_response)){
            return FALSE;
          }else{
            return ($file_contents) ? $file_contents : FALSE;
          }
    }

    public function obtain_provinces_DAO(){
          $json = array();
          $tmp = array();

          $provincias = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Web_1_PHPMarcos/resources/provinciasypoblaciones.xml');
          $result = $provincias->xpath("/lista/provincia/nombre | /lista/provincia/@id");
          for ($i=0; $i<count($result); $i+=2) {
            $e=$i+1;
            $provincia=$result[$e];

            $tmp = array(
              'id' => (string) $result[$i], 'nombre' => (string) $provincia
            );
            array_push($json, $tmp);
          }
              return $json;

    }

    public function obtain_cities_DAO($arrArgument){
          $json = array();
          $tmp = array();

          $filter = (string)$arrArgument;
          $xml = simplexml_load_file($_SERVER['DOCUMENT_ROOT'].'/Web_1_PHPMarcos/resources/provinciasypoblaciones.xml');
          $result = $xml->xpath("/lista/provincia[@id='$filter']/localidades");

          for ($i=0; $i<count($result[0]); $i++) {
              $tmp = array(
                'poblacion' => (string) $result[0]->localidad[$i]
              );
              array_push($json, $tmp);
          }
          return $json;
    }
}
