<?php
/*
$path = $_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos';
define('SITE_ROOT', $path);
require($_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/productsfrontend/model/BLL/productsfrontend.class.singleton.php');
*/
class productsfrontend_model {
    private $bll;
    static $_instance;

    private function __construct() {
        $this->bll = productsfrontend_bll::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }

    public function list_products() {
        return $this->bll->list_products_BLL();
    }
    
    public function details_products($id) {
        return $this->bll->details_products_BLL($id);
    }

    public function list_products_scroll($limit) {
        return $this->bll->list_products_scroll_BLL($limit);
    }
}
