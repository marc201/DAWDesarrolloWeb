<?php
class productsfrontend_DAO {
    static $_instance;

    private function __construct() {
        
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self))
            self::$_instance = new self();
        return self::$_instance;
    }
   
  
    public function list_products_DAO($db) {
        $sql = "SELECT * FROM products";
        $stmt = $db->ejecutar($sql);
        //echo json_encode($db->listar($stmt));
        //die();
        return $db->listar($stmt);
    }
    
    public function details_products_DAO($db,$id) {
        $sql = "SELECT * FROM products WHERE id=".$id;
        $stmt = $db->ejecutar($sql);
        //echo json_encode($db->listar($stmt));
        //die();
        return $db->listar($stmt);
        
    }

    public function list_products_scroll_DAO($db,$limit) {
        $first = $limit['first'];
        $number = $limit['number'];
        $sql = "SELECT * FROM products LIMIT $first,$number";
        $stmt = $db->ejecutar($sql);
        //echo json_encode($db->listar($stmt));
        //die();
        return $db->listar($stmt);
    }
    
}
