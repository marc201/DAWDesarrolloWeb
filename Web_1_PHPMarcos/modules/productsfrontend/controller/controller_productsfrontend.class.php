<?php

class controller_productsfrontend {

    function __construct() {
        //session_start();
    //include ($_SERVER['DOCUMENT_ROOT'] . "/Web_1_PHPMarcos/utils/common.inc.php");
        $_SESSION['module'] = "productsfrontend";
    }

    function list_products() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/productsfrontend/view/', 'list_products.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

    function details_product() {
        require_once(VIEW_PATH_INC . "header.php");
        require_once(VIEW_PATH_INC . "menu.php");

        loadView('modules/productsfrontend/view/', 'details_product.php');

        require_once(VIEW_PATH_INC . "footer.html");
    }

     function load_allproducts() {
        if (isset($_POST["loadproducts"]) && $_POST["loadproducts"] == true) {

            //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products_frontend/model/model/';
            $arrValue = loadModel(MODEL_PRODUCTSFRONTEND, "productsfrontend_model", "list_products");
            
            if ($arrValue) {
                $message = "LOAD PRODUCTS";
            } else {
                $message = "NOT PRODUCTS";
            }

            echo json_encode($arrValue);
            exit();
        }
    }
    function load_products() {
        if (isset($_POST["loadproducts_scroll"]) && $_POST["loadproducts_scroll"] == true) {
            
            
            $initlimitProduct =  $_POST["initlimitProduct"];
            $limitProduct = $_POST["limitProduct"];
            $limit = array(
                    'number' => $_POST["initlimitProduct"],//numero de Registros que saldran
                    'first' => $_POST["limitProduct"]// A partir de que registro comienza
                );
            //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products_frontend/model/model/';

            $arrValue = loadModel(MODEL_PRODUCTSFRONTEND, "productsfrontend_model", "list_products_scroll", $limit);
            
            
            echo json_encode($arrValue);
            exit();
        }
    }

    function load_detailproduct() {
        if (isset($_POST["detailproduct"]) && $_POST["detailproduct"] == true) {
            
           
            $id = $_POST["idProduct"];
            //$path_model=$_SERVER['DOCUMENT_ROOT'] . '/Web_1_PHPMarcos/modules/products_frontend/model/model/';
            $arrValue = loadModel(MODEL_PRODUCTSFRONTEND, "productsfrontend_model", "details_products",$id);

            if ($arrValue) {
                $message = "LOAD PRODUCT";
            } else {
                $message = "NOT PRODUCT";
            }

            $_SESSION["product"] = $arrValue;
            //$_SESSION['msje'] = $mensaje;
            $callback = "../../productsfrontend/details_product/";


            //$jsondata["success"] = true;
            $jsondata["redirect"] = $callback;
            echo json_encode($callback);
            exit();
        }
    }

    function get_product() {
        if (isset($_POST["load"]) && $_POST["load"] == true) {
            $jsondata = array();
            if (isset($_SESSION['product'])) {
                //echo debug($_SESSION['product']);
                $jsondata[0]= $_SESSION['product'];
            }
            /*if (isset($_SESSION['msje'])) {
                //echo $_SESSION['msje'];
                $jsondata["msje"] = $_SESSION['msje'];
            }*/
            //echo($_SESSION['product']);
            close_session();
            echo json_encode($jsondata[0]);
            exit;
        }
    }
}
/*
function close_session() {
    unset($_SESSION['product']);
    unset($_SESSION['msje']);
    $_SESSION = array(); // Destruye todas las variables de la sesión
    session_destroy(); // Destruye la sesión
}
*/