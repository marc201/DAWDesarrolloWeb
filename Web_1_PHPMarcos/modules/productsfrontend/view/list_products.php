<script type="text/javascript" src="<?php echo PRODUCTSFRONTEND_JS_PATH ?>list_products.js" ></script>
<section >
    <div class="container">
        <div id="list_prod" class="row text-center pad-row">
            <ol class="breadcrumb">
                <li class="active" >Products</li>
            </ol>
            <br>
            <div id="content"></div>
            <br>
            <br>
            <br>
        </div>
    </div>
</section>