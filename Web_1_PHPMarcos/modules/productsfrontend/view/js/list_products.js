console.log("entre list!!!");
var limit = 0;
var initlimit = 10;

function load_products() {
    console.log("Voy!")
     $.post("../../productsfrontend/load_allproducts/",{'loadproducts':true}, function (data) {
        var json = JSON.parse(data);
        console.log(json);
        listar_products(json);
        //alert( "success" );
    });
}

function load_products_scroll() {
    console.log("Voy! Scroll")
    $.post("../../productsfrontend/load_products/",{'loadproducts_scroll':true, 'limitProduct':limit, 'initlimitProduct':initlimit}, function (data) {
        var json = JSON.parse(data);
        console.log(json);
        listar_products(json);
        //alert( "success" );
    });

}

function details_product(id) {
    console.log("detail!")
    $.post("../../productsfrontend/load_detailproduct/",{'detailproduct':true, 'idProduct':id}, function (data) {
        var json = JSON.parse(data);
        console.log(json);
        window.location.href = json;
        //alert( "success" );
    });
}

$(document).ready(function () {
    console.log("ready!!!")
    //load_products();
    load_products_scroll();
    limit=limit+5; //Primero saca 10 registros luego avanza de 10 en 10

     $(window).scroll(function () {
  
    if($(window).scrollTop() + $(window).height()+2 >= $(document).height()){

      clearTimeout($.data(this, 'scrollTimer'));
      
       $.data(this, 'scrollTimer', setTimeout(function() {
            initlimit=5;
            limit= limit+5;
            //setTimeout('load_products_scroll()',1000);
            load_products_scroll();
       }, 0)); 
        
      }

  });
    
});

function listar_products(data) {
    //console.log("Cargando...")
    //alert(data.user.avatar);

    var content = document.getElementById("content");

    data.forEach(function (productItem){

        var product = document.createElement("div");
       
       // Insertamos la Imagen
        //var img = document.createElement("div");
        
        var html = '<img src="../../' + productItem.avatar + '" height="75" width="75" align="left"> ';
        product.innerHTML = html;
        //product.appendChild(img);

        product.innerHTML += ("<strong>"+productItem.name + "</strong> <br> LOCATION: "+ productItem.location+ " RELEASE DATE: "+productItem.release_date );
        
        console.log("Cargando...");

        var btn = document.createElement("button");
        var textbtn = document.createTextNode("More info");
        var attbtn = document.createAttribute("id");
        var classbtn = document.createAttribute("class");
        attbtn.value = productItem.id;
        classbtn.value = "detail_product";

        btn.appendChild(textbtn);
        product.appendChild(btn);
        btn.setAttributeNode(attbtn);
        btn.setAttributeNode(classbtn);

        btn.addEventListener("click", function() {
         
          var id = this.getAttribute('id');
          //console.log(id);  
          details_product(id);
        });

        content.appendChild(product);
        var hr = document.createElement("hr");
        content.appendChild(hr);
    });
}