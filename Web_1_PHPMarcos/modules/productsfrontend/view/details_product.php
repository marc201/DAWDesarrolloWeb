<section id="contact-page">
    <div class="container">
        <div class="center">        
            <h2>Detail Product</h2>
            <div id="prod"></div>
        </div> 
        <div class="row contact-wrap"> 
            <div class="status alert alert-success" style="display: none"></div>
            <div id="content"></div>
            <script type="text/javascript" src="<?php echo PRODUCTSFRONTEND_JS_PATH ?>details_product.js" ></script>
        </div><!--/.row-->
    </div><!--/.container-->
</section><!--/#contact-page-->