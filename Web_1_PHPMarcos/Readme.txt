//Version Test 3.2
--(Ajustes Generales)
Ajustes en Amigables para las Pretys en el Main y en el Products
Cambio de las rutas PATHS a HTTP
Cambio de rutas temporal en el Header de acceso a CSS
Cambio rutas PATHS Dropzone

--(Ajustes en modulo Products)
Cambio en el Controler a funciones
Cambio en las rutas a PATHS
Cambio a los metodos Post

//Version Test 3.3
--(Ajustes en modulo Products)
Ajustes del atoload para el el model y 50% del BLL

--(Ajustes en modulo ProductsFrontend)
Cambio en el Controler a funciones
Cambio en las rutas a PATHS
Cambion en el model, bbl y dao al mismo nombre
Cambio a los metodos Post

//Version Master 3.3.2
--(Ajustes en modulo Products)
Arreglos para cargar la imagen el products.

--(Ajustes en modulo ProductsFrontend)
Arreglos para cargar la imagen el details.
Cambio de la lista, incluyendo la imagen.

//Version Test 3.4
--(Ajustes en modulo Contact)
Primera implementacion del modulo

//Cambios para la Proxima Version
El Auload en Model--BBL-DAO y Classe Email