<!DOCTYPE HTML>
<!--
  Arcana by HTML5 UP
  html5up.net | @n33co
  Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html>
  <head>
    <title>WEB|<?php if($_GET['module']){ echo $_GET['module'];}else{ echo "main";} ?> </title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="description" content="" />
    <meta name="keywords" content="" />
    <!--[if lte IE 8]><script src="css/ie/html5shiv.js"></script><![endif]-->
    <script src="<?php echo JS_PATH ?>jquery.min.js"></script>
    <script src="<?php echo JS_PATH ?>jquery.dropotron.min.js"></script>
    <script src="<?php echo JS_PATH ?>skel.min.js"></script>
    <script src="<?php echo JS_PATH ?>skel-layers.min.js"></script>
    <script src="<?php echo JS_PATH ?>init.js"></script>
    <noscript>
      <link rel="stylesheet" href="<?php echo CSS_PATH ?>skel.css" />
      <link rel="stylesheet" href="<?php echo CSS_PATH ?>style.css" />
      <link rel="stylesheet" href="<?php echo CSS_PATH ?>style-wide.css" />
    </noscript>
    <!--[if lte IE 8]><link rel="stylesheet" href="css/ie/v8.css" /><![endif]-->
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  </head>